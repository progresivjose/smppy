<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table){
            $table->string('lastname')->nullable();
            $table->string('ci')->nullable();
            $table->string('ruc')->nullable();
            $table->string('business_name')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('code')->nullable();
            $table->date('birthdate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table){
            $table->dropColumn('lastname');
            $table->dropColumn('ci');
            $table->dropColumn('ruc');
            $table->dropColumn('business_name');
            $table->dropColumn('address');
            $table->dropColumn('phone');
            $table->dropColumn('code');
            $table->dropColumn('birthdate');
        });
    }
}
