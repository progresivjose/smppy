<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $fillable = ['name', 'street1', 'street2', 'street3', 'lat', 'lon', 'type', 'amount'];
}
