<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Wallet extends Model
{
    protected $fillable = ['user_id', 'amount', 'time'];

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
