<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use App\Vehicle;

use App\Wallet;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'lastname', 'ci', 'ruc', 'business_name', 'address', 'phone', 'birthdate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot(){
        parent::boot();

        static::created(function($user){
            $user->assignRole('Automovilista');
        });
    }

    public function vehicles(){
        return $this->belongsToMany(Vehicle::class);
    }

    public function wallet(){
        return $this->hasOne(Wallet::class);
    }
}
