<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penalty extends Model
{
    protected $fillable = ['article_num', 'title', 'description', 'status'];
}
