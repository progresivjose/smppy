<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Vehicle extends Model
{
    protected $fillable = ['plate', 'brand', 'color', 'model', 'chassis', 'document', 'status'];

    public function users(){
    	return $this->belongsToMany(User::class);
    }
}
