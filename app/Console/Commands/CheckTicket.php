<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;

use App\Ticket;

class CheckTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tickets:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica el tiempo fin de los tickets y cambia el estado';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tickets = Ticket::where('status', true)->get();

        foreach ($tickets as $key => $ticket) {
            $endDate = Carbon::parse($ticket->end_date);
            if(Carbon::now()->gt($endDate)){
                echo "El ticket {$ticket->id} ha expirado \n";
                $ticket->status = false;
                $ticket->save();
            } else {
                echo "El ticket {$ticket->id} sigue activo \n";
            }
        }
    }
}
