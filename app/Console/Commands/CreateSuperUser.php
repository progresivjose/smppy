<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

use App\User;

class CreateSuperUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'superuser:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea un usuario con privilegios de super user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "### CREAR SUPER USER ###\n";
        $name     = readline("Introduzca su nombre:");
        $email    = readline("Introduzca su email :");
        echo "Introduzca su password: ";
        $this->hide_term();
        $password = rtrim(fgets(STDIN), PHP_EOL);
        $this->restore_term();
        echo "\n";
        //validar los inputs
        if(empty($name))
          exit("El nombre está vacio\n");
        if(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL))
          exit("El email no es correcto\n");
        if(empty($password) || strlen($password) < 5)
          exit("El password debe tener al menos 5 caracteres\n");


        $name     = filter_var($name, FILTER_SANITIZE_STRING);
        $password = Hash::make($password);

        //validar que el email no existe
        $user = User::where('email', '=', $email)->first();
        if(!$user){
          $user = User::create([
            'name'         => $name,
            'email'        => $email,
            'password'     => $password
          ]);
          $user->superuser = true;
          $user->save();
          $user->assignRole('admin');
          exit("El usuario ha sido creado!\n");
        } else {
          exit("El Usuario ya existe!\n");
        }
    }

    private function hide_term() {
        if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')
            system('stty -echo');
    }

    private function restore_term() {
        if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')
            system('stty echo');
    }
}
