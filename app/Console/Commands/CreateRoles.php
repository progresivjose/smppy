<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roles:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea los roles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        Permission::create(['name' => 'index_users']);
        Permission::create(['name' => 'edit_users']);
        Permission::create(['name' => 'create_users']);
        Permission::create(['name' => 'delete_users']);
        Permission::create(['name' => 'index_penalties']);
        Permission::create(['name' => 'edit_penalties']);
        Permission::create(['name' => 'create_penalties']);
        Permission::create(['name' => 'delete_penalties']);

        //ADMIN
        $admin = Role::create(['name' => 'admin']);
        $admin->givePermissionTo('index_users');
        $admin->givePermissionTo('edit_users');
        $admin->givePermissionTo('create_users');
        $admin->givePermissionTo('delete_users');


        //PMT
        $pmt = Role::create(['name' => 'pmt']);
        $pmt->givePermissionTo('index_penalties');
        $pmt->givePermissionTo('edit_penalties');
        $pmt->givePermissionTo('create_penalties');
        $pmt->givePermissionTo('delete_penalties');

        //Cuida Coches
        $pmt = Role::create(['name' => 'Cuida Coches']);

        //Supervisor
        $pmt = Role::create(['name' => 'Supervisor']);

        //Automovilistas
        $pmt = Role::create(['name' => 'Automovilista']);

    }
}
