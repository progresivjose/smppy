<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Zone;

class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $zones = Zone::paginate(15);

        return view('zones.index', ['zones' => $zones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('zones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'    => 'string|required|max:255',
            'street1' => 'string|required|max:255',
            'street2' => 'string|required|max:255',
            'street3' => 'string|required|max:255',
            'lat'     => 'string',
            'lon'     => 'string',
            'type'    => 'integer|required',
            'amount'  => 'integer|min:0'
        ]);

        $zone = Zone::create($request->only(['name', 'street1', 'street2', 'street3', 'lat', 'lon', 'type', 'amount']));

        if($zone){
            flash('La zona ha sido creada!')->success();
            return redirect()->route('zonas.index');
        } else {
            flash('La zona no pudo ser creada!')->danger();
            return redirect()->route('zonas.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $zone = Zone::find($id);

        return view('zones.edit', ['zone' => $zone]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'    => 'string|required|max:255',
            'street1' => 'string|required|max:255',
            'street2' => 'string|required|max:255',
            'street3' => 'string|required|max:255',
            'lat'     => 'string',
            'lon'     => 'string',
            'type'    => 'integer|required',
            'amount'  => 'integer|min:0'
        ]);

        $zone = Zone::find(1);

        if($zone->update($request->only(['name', 'street1', 'street2', 'street3', 'lat', 'lon', 'type', 'amount']))){
            flash('La zona ha sido modificada!')->success();
            return redirect()->route('zonas.index');
        } else {
            flash('La zona no pudo ser modificada!')->danger();
            return redirect()->route('zonas.edit', $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $zone = Zone::find($id);

        if($zone->delete()){
            return response()->json(['success' => true, 'message' => 'La zona ha sido eliminada']);
        } else {
            return response()->json(['success' => false, 'message' => 'La zona no puede ser eliminada']);
        }
    }
}
