<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Penalty;

class PenaltyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penalties = Penalty::paginate(15);

        return view('penalties.index', ['penalties' => $penalties]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penalties.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'article_num' => 'string|required|max:4',
            'title'       => 'string|required|max:255',
            'description' => 'string|required',
        ]);

        $penalty = Penalty::create($request->only(['article_num', 'title', 'description']));
        if($penalty){
            flash('La multa ha sido creada!')->success();
            return redirect()->route('multas.index');
        } else {
            flash('La multa no pudo  ser creada!')->danger();
            return redirect()->route('multas.create');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penalty = Penalty::find($id);

        return view('penalties.edit', ['penalty' => $penalty]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'article_num' => 'string|required|max:4',
            'title'       => 'string|required|max:255',
            'description' => 'string|required',
        ]);

        $penalty = Penalty::find($id);

        if($penalty->update($request->only(['article_num', 'title', 'description']))){
            flash('La multa ha sido modificada!')->success();
            return redirect()->route('multas.index');
        } else {
            flash('La multa no pudo ser modificada!')->danger();
            return redirect()->route('multas.edit', $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penalty = Penalty::find($id);

        if($penalty->delete()){
            return response()->json(['success' => true, 'message' => 'La multa ha sido eliminada']);
        } else {
            return response()->json(['success' => false, 'message' => 'La multa no puede ser eliminada']);
        }
    }
}
