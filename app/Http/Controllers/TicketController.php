<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use Carbon\Carbon;

use App\Wallet;

use App\Plan;

use App\Ticket;

use App\Vehicle;

use App\PaymentMethod;


class TicketController extends Controller
{
    public function time(Request $request){
    	$plans = Plan::orderBy('description', 'asc')->get();
        $wallet = Auth::user()->wallet()->first();
    	return view('tickets.buy_time', ['plans' => $plans, 'wallet' => $wallet]);
    }

    public function buyTime(Request $request){

    	$this->validate($request, [
            'plan'       => 'required|integer|exists:plans,id'
    	]);

		$plan   = Plan::find($request->get('plan'));
		$user   = Auth::user();
		$wallet = Wallet::where('user_id', $user->id)->first();
		if($wallet){
			//aumenta la cantidad de minutos comprados
			$wallet->time   = $wallet->time + $plan->minutes;
			//disminulle la cantidad de dinero que tiene en su billetera
			$wallet->amount = $wallet->amount - $plan->amount;

			if($wallet->amount >= 0){
				$wallet->save();
				flash('La compra de minutos ha sido exitosa!')->success();
				return redirect('/');
			} else {
				flash('Su billetera no posee la cantidad suficiente para realizar la compra de minutos')->error();
				return redirect()->route('time');
			}
		} else {
			flash('Usted no posee aún una billetera!')->warning();

			return redirect()->route('wallet');
		}
    }

    public function showMinutes(){
    	$user = Auth::user();

    	$wallet = Wallet::where('user_id', $user->id)->first();

    	return view('tickets.show_minutes', ['wallet' => $wallet]);
    }

    public function showWallet(Request $request){
		$methods = PaymentMethod::all();
		$wallet  = Wallet::where('user_id', Auth::user()->id)->first();
    	return view('tickets.buy_wallet',['wallet' => $wallet, 'methods' => $methods]);
    }

    public function buyWallet(Request $request){
    	$this->validate($request, [
    		'amount' => 'required|integer|min:1000',
    		'method' => 'required|integer|exists:payment_methods,id'
    	]);

    	$wallet = Wallet::where('user_id', Auth::user()->id)->first();

    	if(!$wallet){
    		//en caso que no exista la billetera debe crease
    		$wallet = Wallet::create(['user_id' => Auth::user()->id, 'amount' => 0, 'time' => 0]);
    	}

    	/**
    	 * El proceso debe redireccionar a una pagina de simulacro para realizar la compra
    	 * una vez finalizado el simulacro debe devolver a otra página donde carga los datos
    	 * que fueron pasados por dicha página y guardarlos en la base de datos
    	 */

    	$wallet->amount = $request->amount;
    	if($wallet->save()){
    		flash('La carga de la billetera fue exitosa!')->success();
    		return redirect()->route('time');
    	} else {
    		flash('Ocurrió un problema al realizar la carga en la billetera!')->danger();
    		return redirect()->route('wallet');
    	}
    }

    function tickets(Request $request){
        $wallet   = Wallet::where('user_id', Auth::user()->id)->first();
        $vehicles = Auth::user()->vehicles()->get();

        return view('tickets.buy_tickets', ['wallet' => $wallet, 'vehicles' => $vehicles]);
    }

    function buyTicket(Request $request){
        $this->validate($request, [
            'minutes' => 'required|integer|min:0',
            'vehicle' => 'required|integer|exists:vehicles,id',
            'lat'     => 'required|string',
            'lon'     => 'required|string'
        ]);

        $wallet   = Wallet::where('user_id', Auth::user()->id)->first();
        $timeLeft = $wallet->time - $request->minutes;

        if($timeLeft < 0){
            flash('No posee la cantidad de minutos para este ticket')->error();
            return redirect()->route('tickets');
        }


        //extraer los minutos de la billetera
        $wallet->time = $timeLeft;
        $wallet->save();

        //generar boleta
        $iniDate = Carbon::now();
        $endDate = Carbon::now()->addMinutes($request->minutes);
        $ticket  = Ticket::create(['ini_date' => $iniDate, 'end_date' => $endDate, 'wallet_id' => $wallet->id, 'vehicle_id' => $request->vehicle, 'lat' => $request->lat, 'lon' => $request->lon]);

        if($ticket){
            flash('El ticket ha sido generado, su tiempo de estacionamiento ya se encuentra corriendo')->success();
            return redirect('/');
        } else {
            flash('Ocurrió un problema al generar el ticket, intente nuevamente')->error();
            return redirect()->route('tickets');
        }

    }
}
