<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Vehicle;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user     = Auth::user();
        $vehicles = $user->vehicles()->paginate(15);

        return view('vehicles.index', ['vehicles' => $vehicles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vehicles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'plate'    => 'required|string',
            'brand'    => 'required|string',
            'color'    => 'required|string',
            'model'    => 'required|string',
            'chassis'  => 'string',
            'document' => 'required|string',
            'status'   => 'required|integer'
        ]);

        $user = Auth::user();

        $vehicle = Vehicle::where('plate', $request->plate)->first();

        if(!$vehicle){
            $vehicle = Vehicle::create($request->only(['plate', 'brand', 'color', 'model', 'chassis', 'document', 'status']));
        }

        if($vehicle){
            $user->vehicles()->attach($vehicle);

            flash('El vehiculo ha sido creado')->success();
            return redirect()->route('mis-vehiculos.index');
        } else {
            flash('El vehiculo no pudo ser creado')->error();
            return redirect()->route('mis-vehiculos.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicle = Vehicle::find($id);
        return view('vehicles.edit', ['vehicle' => $vehicle]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'plate'    => 'required|string',
            'brand'    => 'required|string',
            'color'    => 'required|string',
            'model'    => 'required|string',
            'chassis'  => 'string',
            'document' => 'required|string',
            'status'   => 'required|integer'
        ]);

        $vehicle = Vehicle::find($id);

        if($vehicle->update($request->only(['plate', 'brand', 'color', 'model', 'chassis', 'document', 'status']))){
            flash('El vehiculo ha sido modificado')->success();
            return redirect()->route('mis-vehiculos.index');
        } else {
            flash('El vehiculo no pudo ser modificado')->error();
            return redirect()->route('mis-vehiculos.update', $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user    = Auth::user();
        $vehicle = Vehicle::find($id);

        if($user->vehicles()->detach($vehicle)){
            return response()->json(['success' => true, 'message' => 'El vehículo ha sido eliminado de tu listado']);
        } else {
            return response()->json(['success' => false, 'message' => 'El vehículo no pudo ser eliminado de tu listado']);
        }
    }
}
