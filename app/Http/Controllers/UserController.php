<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;

use Spatie\Permission\Models\Role;

use Auth;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('status', true)->paginate(15);
        return view('users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all()->pluck('name', 'name');
        return view('users.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'string|required',
            'lastname'      => 'string|required',
            'email'         => 'email|required|unique:users',
            'ci'            => 'string|required',
            'ruc'           => 'string|required',
            'business_name' => 'string',
            'address'       => 'string',
            'phone'         => 'string',
            'birthdate'     => 'date',
        ]);

        $pass             = str_random(8);
        $password         = Hash::make($pass);
        $data             = $request->only(['name', 'lastname','email', 'ci', 'ruc', 'business_name', 'address', 'phone', 'birthdate']);
        $data['password'] = $password;
        $user = User::create($data);
        if($user){
            //asigar el rol
            $user->assignRole($request->get('rol'));
            //enviar email al usuario
            flash('El usuario ha sido guardado, password: ' . $pass)->success();
            return redirect()->route('usuarios.index');
        } else {
            flash('El usuario no pudo ser creado')->error();
            return redirect()->route('usuarios.create');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        $roles = Role::all()->pluck('name', 'name');
        return view('users.edit', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'string|required',
            'lastname'      => 'string|required',
            'email'         => 'email|required',
            'ci'            => 'string|required',
            'ruc'           => 'string|required',
            'business_name' => 'string',
            'address'       => 'string',
            'phone'         => 'string',
            'birthdate'     => 'date',
        ]);

        $user = User::find($id);

        if($user->update($request->only(['name', 'lastname','email', 'ci', 'ruc', 'business_name', 'address', 'phone', 'birthdate']))){
            $roles = Role::all();
            foreach ($roles as $key => $role) {
                $user->removeRole($role['name']);
            }
            $user->assignRole($request->get('rol'));
            flash('El usuario ha sido modificado')->success();
            return redirect()->route('usuarios.index');
        } else {
            flash('El usuario no pudo ser modificado')->danger();
            return redirect()->route('usuarios.edit', $user->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user         = User::find($id);
        $user->status = false;
        if($user->update()){
            return response()->json(['success' => true, 'message' => 'El usuario ha sido eliminado']);
        } else {
            return response()->json(['success' => false, 'message' => 'El usuario no puede ser eliminado']);
        }
    }

    public function account(){
        $user = Auth::user();

        return view('users.account', ['user' => $user]);
    }

    public function updateAccount(Request $request){
        $this->validate($request, [
            'name'          => 'string|required',
            'lastname'      => 'string|required',
            'email'         => 'email|required|unique:users',
            'ci'            => 'string|required',
            'ruc'           => 'string|required',
            'business_name' => 'string',
            'address'       => 'string',
            'phone'         => 'string',
            'birthdate'     => 'date',
        ]);

        $user = Auth::user();

        if($user->update($request->only(['name', 'lastname','email', 'ci', 'ruc', 'business_name', 'address', 'phone', 'birthdate']))){
            flash('La cuenta ha sido modificada')->success();
        }  else {
            flash('La cuenta no pude ser modificada')->danger();
        }

        return redirect()->route('account');
    }
}
