<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Ticket;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wallet = Auth::user()->wallet()->first();
        $ticket = Ticket::whereRaw('wallet_id = ? AND status = TRUE', [$wallet->id])->first();
        return view('home.index', ['ticket' => $ticket]);
    }
}
