<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['ini_date', 'end_date', 'wallet_id', 'vehicle_id', 'lat', 'lon'];
}
