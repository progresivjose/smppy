@extends('layouts.auth')

@section('content')

 <div class="containe mlogin">
    <div id="login">
        <h3>SMPPy</h3>
            <form class="form-horizontal" id="loginform" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="ci" type="text" class="form-control" name="email" value="{{ old('ci') }}" required autofocus>

                        @if ($errors->has('ci'))
                            <span class="help-block">
                                <strong>{{ $errors->first('ci') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Ingresar
                        </button>
                        <p class="regtext">No estas registrado? <a href="#" >Registrate Aquí</a>!</p>
                        <p class="regtext">No recuerdas tu contraseña? <a class="btn btn-link" href="{{ url('/password/reset') }}"> Restablecer</a>
                    </div>
                </div>
            </form>
    </div>
</div>

@endsection
