@extends('layouts.app')

@section('content')

	<h3>Planes</h3>

	<a href="{{ route('metodos.create')}}" class="btn btn-success">Agregar Metodo de Pago</a>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Descripcion</th>
				<th>URL</th>
				<th>Estado</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			@foreach ($methods as $method)
				<tr data-id="{{ $method->id }}">
					<td>{{ $method->title }}</td>
					<td>{{ $method->url }}</td>
					<td>{{ $method->status }}</td>
					<td>
						<a href="{{ route('metodos.edit', $method->id)}}">Editar</a>
						<a href="#" class="btn-remove">Eliminar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	{!! $methods->links() !!}

	{!! Form::open(['route' => ['metodos.destroy',':ROW_ID'], 'method' => 'DELETE', 'id' => 'form-delete']) !!}
	{!! Form::close() !!}

@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('.btn-remove').click(function(e){
			e.preventDefault();
			var row = $(this).parents('tr');
			var id = row.data('id');
			swal({
			  title: "Estas seguro de eliminar el metodo de pago?",
			  text: "Con esta acción los datos no podrán ser recuperados!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Eliminar!",
			  cancelButtonText: "Cancelar!"
			}).then(function(isConfirm){
				console.log(isConfirm);
			  if (isConfirm) {
				var form = $('#form-delete');
				var url = form.attr('action').replace(':ROW_ID',id);
				var data = form.serialize();
				var type = "";
				var title = "";
			  	$.post(url,data, function(result){
					if(result.success){
						row.fadeOut();
						type = "success";
						title = "Operación realizada!";
					}else{
						type = "error";
						title =  "No se pudo realizar la operación";
					}
					swal({   title: title,   text: result.message,   type: type,   confirmButtonText: "Aceptar" });
				}).fail(function (){
					swal('No se pudo realizar la petición.');
				});
			  } else {
			    swal("Cancelado", "El metodo de pago no ha sido eliminado", "error");
			  }
			});
		});
	});
</script>
@endsection