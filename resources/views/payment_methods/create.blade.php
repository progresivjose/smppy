@extends('layouts.app')

@section('content')

	<h3>Agregar Metodo de Pago</h3>

	{!! Form::open(['route' => 'metodos.store', 'method' => 'POST', 'role' => 'form']) !!}
		<div class="form-group">
			{!! Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Descripción'] )!!}

			@if ($errors->has('title'))
	        <span class="help-block">
	            <strong>{{ $errors->first('title') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			{!! Form::text('url', '', ['class' => 'form-control', 'placeholder' => 'URL'] )!!}

			@if ($errors->has('url'))
	        <span class="help-block">
	            <strong>{{ $errors->first('url') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			<label for="status">Estado</label>
			{!! Form::select('status', [true => 'Activo', false => 'Inactivo'], true, ['class' => 'form-control', 'id' => 'status'] )!!}
			@if ($errors->has('status'))
	        <span class="help-block">
	            <strong>{{ $errors->first('status') }}</strong>
	        </span>
	        @endif
		</div>

		<div class="form-group">
			{!! Form::submit('Guardar', ['class' => 'btn btn-success'] )!!}
			<a href="{{ route('planes.index')}}" class="btn btn-danger pull-right">Cancelar</a>
		</div>

	{!! Form::close() !!}

@endsection