@extends('layouts.app')

@section('content')
<div id="welcome">
	<h2>Bienvenido, <span>{{ Auth::user()->name . ' ' . Auth::user()->lastname}}! </span></h2>

	@if ($ticket)
		<div class="alert alert-warning">
			<p><strong>Tiene un ticket en uso</strong></p>
			<p>Fecha inicio: {{ $ticket->ini_date }}</p>
			<p>Fecha fin: {{ $ticket->end_date }}</p>
			<p>Minutos restantes: {{ \Carbon\Carbon::now()->diffInMinutes(\Carbon\Carbon::parse($ticket->end_date)) }}</p>
		</div>
	@endif

	<div class="actions">
		<a href="{{ route('time')}}" class="btn btn-success btn-lg">Comprar Minutos</a>
	</div>
	<div class="actions">
		<a href="{{ route('wallet')}}" class="btn btn-primary btn-lg">Cargar Billetera</a>
	</div>
	<div class="actions">
		<a href="{{ route('tickets')}}" class="btn btn-warning btn-lg">Comprar Ticket</a>
	</div>
</div>
@endsection