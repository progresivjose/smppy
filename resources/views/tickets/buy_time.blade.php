@extends('layouts.app')

@section('content')
<div id="welcome">
	<h2>Compra de minutos</h2>
	<small>Usuario: {{ Auth::user()->name . ' ' . Auth::user()->lastname}}</small>

	<p>Monto de Billetera: <strong>{{ number_format($wallet->amount, 0, ',', '.') }}Gs</strong></p>
	<p>Tiempo comprado en minutos: <strong>{{ $wallet->time }} min.</strong></p>

	<fieldset>
		<legend>Monto en minutos</legend>
		{!! Form::open(['route' => 'buy_time', 'method' => 'POST', 'role' => 'form'])!!}
		<div class="row">
			@foreach ($plans as $plan)
				<div class="col-md-4">
					<h3>{{ $plan->description }}</h3>
					<h4>{{ $plan->minutes }} min.</h4>
					<small>A solo <strong>{{ number_format($plan->amount, 0,',', '.') }}Gs</strong></small>
					<div class="form-group">
						<label>{!! Form::radio('plan', $plan->id) !!}</label>
					</div>
				</div>
			@endforeach
		</div>

		<div class="form-group">
			{!! Form::submit('Comprar Minutos', ['class' => 'btn btn-success btn-lg pull-right'])!!}
		</div>
		{!! Form::close()!!}
	</fieldset>
</div>
@endsection