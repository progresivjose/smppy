@extends('layouts.app')

@section('content')
<div id="welcome">
	<h2>Cargar a billetera</h2>
	<small>Usuario: {{ Auth::user()->name . ' ' . Auth::user()->lastname}}</small>

	@if ($wallet)
		<p>Monto de Billetera: <strong>{{ number_format($wallet->amount, 0, ',', '.') }}Gs</strong></p>
		<p>Tiempo comprado en minutos: <strong>{{ $wallet->time }} min.</strong></p>
	@else
		<div class="alert alert-warning">
			<strong>Usted aún no posee una billetera!</strong>
			<p>Realice una carga para poder obtener una billetera y así poder comprar minutos</p>
		</div>
	@endif

	<fieldset>
		<legend>Cargar</legend>
		{!! Form::open(['route' => 'buy_wallet', 'method' => 'post', 'role' => 'form'])!!}
			<div class="form-group">
				<label for="amount">Monto</label>
				{!! Form::number('amount', 0, ['class' => 'form-control', 'required' => true, 'id' => 'amount']) !!}
			</div>

			<h4>Metodo de pago</h4>
			@foreach ($methods as $method)
			<div class="form-group">
				<label>{!! Form::radio('method', $method->id) !!} {{ $method->title }}</label>
			</div>
			@endforeach

			<div class="form-group">
				{!! Form::submit('Cargar',['class' => 'btn btn-success btn-lg pull-right'])!!}
			</div>
		{!! Form::close() !!}
	</fieldset>

</div>
@endsection