@extends('layouts.app')

@section('content')
<div id="welcome">
	<h2>Generar Ticket</h2>

	@if ($wallet)
		<p>Minutos a favor: <strong id="show-own-time">{{ $wallet->time }} min.</strong></p>
		<input type="hidden" id="own-time" value="{{ $wallet->time }}">
	@else
		<div class="alert alert-warning">
			<strong>Usted aún no posee una billetera!</strong>
			<p>Realice una carga para poder obtener una billetera y así poder comprar minutos</p>
		</div>
	@endif

	@include('partials.errors')

	{!! Form::open(['route' => 'buy_tickets', 'method' => 'POST', 'role' => 'form'])!!}
		<div class="form-group">
			<label>Cantidad de minutos</label>
			{!! Form::number('minutes', 0, ['class' => 'form-control', 'required' => true, 'id' => 'buy-time'])!!}
		</div>

		<div class="form-group">
			<h4>Elegir vehículo</h4>
			<table class="table">
				<tbody>

				@foreach ($vehicles as $vehicle)
					<tr>
						<td>{{ $vehicle->plate }}</td>
						<td>{{ $vehicle->brand }}</td>
						<td>{{ $vehicle->model }}</td>
						<td>{{ $vehicle->color }}</td>
						<td>{!! Form::radio('vehicle', $vehicle->id) !!}</td>
					</tr>
				@endforeach
				</tbody>
			</table>

		</div>

		<div class="form-group">
			{!! Form::submit('Generar', ['class' => 'btn btn-success btn-lg'])!!}
		</div>

		{!! Form::hidden('lat', '', ['id' => 'latitud']) !!}
		{!! Form::hidden('lon', '', ['id' => 'longitud']) !!}
	{!! Form::close() !!}


</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#buy-time').change(function(e){
				e.preventDefault();
				if($(this).val() > 0){
					var ownTime  = $('#own-time').val();
					var timeLeft = ownTime - $(this).val();
					if(timeLeft >= 0){
						$('#show-own-time').html(timeLeft + ' min,');
					} else {
						$('#show-own-time').html(timeLeft + ' min,');
						$(this).val(ownTime);
					}
				} else {
					$('#show-own-time').html($('#own-time').val() + ' min,');
					$(this).val(0);
				}
			});

			getLocation();
		});
	</script>
@endsection