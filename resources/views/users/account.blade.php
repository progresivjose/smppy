@extends('layouts.app')

@section('content')
	<h3>Mi Perfil</h3>

	{!! Form::open(['route' => ['update_account', $user->id], 'method' => 'PUT', 'role' => 'form']) !!}
	<div class="form-group">
		{!! Form::text('name', $user->name , ['class' => 'form-control', 'required' => true, 'placeholder' => 'Nombre'])!!}

		@if ($errors->has('name'))
	        <span class="help-block">
	            <strong>{{ $errors->first('name') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::text('lastname', $user->lastname, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Apellido'])!!}

		@if ($errors->has('lastname'))
	        <span class="help-block">
	            <strong>{{ $errors->first('lastname') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::email('email', $user->email, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Email'])!!}

		@if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
	</div>

	<div class="form-group">
		{!! Form::text('ci', $user->ci, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Ci'])!!}

		@if ($errors->has('ci'))
	        <span class="help-block">
	            <strong>{{ $errors->first('ci') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::text('ruc', $user->ruc, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Ruc'])!!}
	</div>

	<div class="form-group">
		{!! Form::text('business_name', $user->business_name, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Razon Social'])!!}
	</div>

	<div class="form-group">
		{!! Form::text('address', $user->address, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Dirección'])!!}
	</div>

	<div class="form-group">
		{!! Form::text('phone', $user->phone, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Teléfono'])!!}
	</div>

	<div class="form-group">
		{!! Form::date('birthdate', $user->birthdate, ['class' => 'form-control']); !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Guardar', ['class' => 'btn btn-success']); !!}
		<a href="{{ url('/') }}" class="btn btn-danger pull-right">Cancelar</a>
	</div>



	{!! Form::close() !!}
@endsection