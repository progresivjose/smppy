@extends('layouts.app')

@section('content')

<h3>Usuarios</h3>

<a href="{{ route('usuarios.create') }}" class="btn btn-success">Agregar Usuario</a>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>CI</th>
			<th>Email</th>
			<th>Super User</th>
			<th></th>
		</tr>
	</thead>

	<tbody>
		@foreach ($users as $user)
			<tr data-id="{{ $user->id }}">
				<td>{{ $user->name }}</td>
				<td>{{ $user->lastname }}</td>
				<td>{{ $user->ci }}</td>
				<td>{{ $user->email }}</td>
				<td>
					<a href="{{ route('usuarios.edit', $user) }}">Editar</a>
					&nbsp;
					<a href="#" class="btn-remove">Bloquear</a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $users->links() !!}

{!! Form::open(['route' => ['usuarios.destroy',':ROW_ID'], 'method' => 'DELETE', 'id' => 'form-delete']) !!}
{!! Form::close() !!}

@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('.btn-remove').click(function(e){
			e.preventDefault();
			var row = $(this).parents('tr');
			var id = row.data('id');
			swal({
			  title: "Estas seguro que desea bloquear al usuario?",
			  text: "Con esta acción los datos no podrán ser recuperados!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Bloquear!",
			  cancelButtonText: "Cancelar!"
			}).then(function(isConfirm){
				console.log(isConfirm);
			  if (isConfirm) {
				var form = $('#form-delete');
				var url = form.attr('action').replace(':ROW_ID',id);
				var data = form.serialize();
				var type = "";
				var title = "";
			  	$.post(url,data, function(result){
					if(result.success){
						row.fadeOut();
						type = "success";
						title = "Operación realizada!";
					}else{
						type = "error";
						title =  "No se pudo realizar la operación";
					}
					swal({   title: title,   text: result.message,   type: type,   confirmButtonText: "Aceptar" });
				}).fail(function (){
					swal('No se pudo realizar la petición.');
				});
			  } else {
			    swal("Cancelado", "El usuario no pudo ser bloqueado", "error");
			  }
			});
		});
	});
</script>
@endsection