@extends('layouts.app')

@section('content')

	<h3>Planes</h3>

	<a href="{{ route('planes.create')}}" class="btn btn-success">Agregar Plan</a>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Descripcion</th>
				<th>Monto</th>
				<th>Minutos</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			@foreach ($plans as $plan)
				<tr data-id="{{ $plan->id }}">
					<td>{{ $plan->description }}</td>
					<td>{{ $plan->amount }}</td>
					<td>{{ $plan->minutes }}</td>
					<td>
						<a href="{{ route('planes.edit', $plan->id)}}">Editar</a>
						<a href="#" class="btn-remove">Eliminar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	{!! $plans->links() !!}

	{!! Form::open(['route' => ['planes.destroy',':ROW_ID'], 'method' => 'DELETE', 'id' => 'form-delete']) !!}
	{!! Form::close() !!}

@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('.btn-remove').click(function(e){
			e.preventDefault();
			var row = $(this).parents('tr');
			var id = row.data('id');
			swal({
			  title: "Estas seguro de eliminar el plan?",
			  text: "Con esta acción los datos no podrán ser recuperados!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Eliminar!",
			  cancelButtonText: "Cancelar!"
			}).then(function(isConfirm){
				console.log(isConfirm);
			  if (isConfirm) {
				var form = $('#form-delete');
				var url = form.attr('action').replace(':ROW_ID',id);
				var data = form.serialize();
				var type = "";
				var title = "";
			  	$.post(url,data, function(result){
					if(result.success){
						row.fadeOut();
						type = "success";
						title = "Operación realizada!";
					}else{
						type = "error";
						title =  "No se pudo realizar la operación";
					}
					swal({   title: title,   text: result.message,   type: type,   confirmButtonText: "Aceptar" });
				}).fail(function (){
					swal('No se pudo realizar la petición.');
				});
			  } else {
			    swal("Cancelado", "El plan no ha sido eliminado", "error");
			  }
			});
		});
	});
</script>
@endsection