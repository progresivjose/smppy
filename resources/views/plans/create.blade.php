@extends('layouts.app')

@section('content')

	<h3>Agregar Plan</h3>

	{!! Form::open(['route' => 'planes.store', 'method' => 'POST', 'role' => 'form']) !!}
		<div class="form-group">
			{!! Form::text('description', '', ['class' => 'form-control', 'placeholder' => 'Descripción'] )!!}

			@if ($errors->has('description'))
	        <span class="help-block">
	            <strong>{{ $errors->first('description') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			<label for="minutes">Minutos</label>
			{!! Form::number('minutes', 0, ['class' => 'form-control', 'placeholder' => 'Minutos', 'id' => 'minutes'] )!!}
			@if ($errors->has('minutes'))
	        <span class="help-block">
	            <strong>{{ $errors->first('minutes') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			<label for="amount">Monto</label>
			{!! Form::number('amount', 0, ['class' => 'form-control', 'placeholder' => 'Monto', 'id' => 'amount'] )!!}
			@if ($errors->has('amount'))
	        <span class="help-block">
	            <strong>{{ $errors->first('amount') }}</strong>
	        </span>
	        @endif
		</div>

		<div class="form-group">
			<label for="status">Estado</label>
			{!! Form::select('status', [true => 'Activo', false => 'Inactivo'], true, ['class' => 'form-control', 'id' => 'status'] )!!}
			@if ($errors->has('status'))
	        <span class="help-block">
	            <strong>{{ $errors->first('status') }}</strong>
	        </span>
	        @endif
		</div>

		<div class="form-group">
			{!! Form::submit('Guardar', ['class' => 'btn btn-success'] )!!}
			<a href="{{ route('planes.index')}}" class="btn btn-danger pull-right">Cancelar</a>
		</div>

	{!! Form::close() !!}

@endsection