@extends('layouts.app')

@section('content')

	<h3>Tipo de Multas</h3>

	<a href="{{ route('multas.create')}}" class="btn btn-success">Agregar Multa</a>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Articulo</th>
				<th>Titulo</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			@foreach ($penalties as $penalty)
				<tr data-id="{{ $penalty->id }}">
					<td>{{ $penalty->article_num }}</td>
					<td>{{ $penalty->title }}</td>
					<td>
						<a href="{{ route('multas.edit', $penalty->id)}}">Editar</a>
						<a href="#" class="btn-remove">Eliminar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	{!! $penalties->links() !!}

	{!! Form::open(['route' => ['multas.destroy',':ROW_ID'], 'method' => 'DELETE', 'id' => 'form-delete']) !!}
	{!! Form::close() !!}

@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('.btn-remove').click(function(e){
			e.preventDefault();
			var row = $(this).parents('tr');
			var id = row.data('id');
			swal({
			  title: "Estas seguro de eliminar ĺa multa?",
			  text: "Con esta acción los datos no podrán ser recuperados!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Eliminar!",
			  cancelButtonText: "Cancelar!"
			}).then(function(isConfirm){
				console.log(isConfirm);
			  if (isConfirm) {
				var form = $('#form-delete');
				var url = form.attr('action').replace(':ROW_ID',id);
				var data = form.serialize();
				var type = "";
				var title = "";
			  	$.post(url,data, function(result){
					if(result.success){
						row.fadeOut();
						type = "success";
						title = "Operación realizada!";
					}else{
						type = "error";
						title =  "No se pudo realizar la operación";
					}
					swal({   title: title,   text: result.message,   type: type,   confirmButtonText: "Aceptar" });
				}).fail(function (){
					swal('No se pudo realizar la petición.');
				});
			  } else {
			    swal("Cancelado", "La multa no ha sido eliminada", "error");
			  }
			});
		});
	});
</script>
@endsection