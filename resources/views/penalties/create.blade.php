@extends('layouts.app')

@section('content')

	<h3>Agregar Multa</h3>

	{!! Form::open(['route' => 'multas.store', 'method' => 'POST', 'role' => 'form']) !!}
		<div class="form-group">
			{!! Form::text('article_num', '', ['class' => 'form-control', 'placeholder' => 'Artículo'] )!!}

			@if ($errors->has('article_num'))
	        <span class="help-block">
	            <strong>{{ $errors->first('article_num') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			{!! Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Nombre'] )!!}
			@if ($errors->has('title'))
	        <span class="help-block">
	            <strong>{{ $errors->first('title') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			{!! Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'Descripción'] )!!}
			@if ($errors->has('description'))
	        <span class="help-block">
	            <strong>{{ $errors->first('description') }}</strong>
	        </span>
	        @endif
		</div>

		<div class="form-group">
			{!! Form::submit('Guardar', ['class' => 'btn btn-success'] )!!}
			<a href="{{ route('multas.index')}}" class="btn btn-danger pull-right">Cancelar</a>
		</div>

	{!! Form::close() !!}

@endsection