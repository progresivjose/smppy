<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ url('/')}}">SMPPy</a>
		</div>
		<ul class="nav navbar-nav">
			@role('admin')
				<li>
					<a href="{{ route('usuarios.index') }}">Listar Usuarios</a>
				</li>
				<li>
					<a href="{{ route('multas.index') }}">Listar Multas</a>
				</li>
				<li>
					<a href="{{ route('zonas.index') }}">Listar Zonas</a>
				</li>
				<li>
					<a href="{{ route('planes.index') }}">Listar Planes</a>
				</li>

				<li>
					<a href="{{ route('metodos.index') }}">Metodos de Pago</a>
				</li>
			@endrole

			@role('pmt')
				<li>
					<a href="{{ route('multas.index') }}">Listar Multas</a>
				</li>
			@endrole

				<li>
					<a href="{{ route('mis-vehiculos.index')}}">Mis Vehículos</a>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">Billetera <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="{{ route('wallet') }}">Carga de Billetera</a></li>
						<li><a href="{{ route('time') }}">Compra de Minutos</a></li>
						<li><a href="{{ route('tickets') }}">Compra de Tickets</a></li>
					</ul>
      			</li>

			<li class="pull-right">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              <span class="hidden-xs">{{ Auth::user()->name }}</span>
	            </a>
				<ul class="dropdown-menu">
	              <!-- User image -->
	              <li class="user-header">
	                <p>
	                  {{ Auth::user()->name . ' ' . Auth::user()->lastname }}
	                  <small>Miembro desde {{ Auth::user()->created_at->format('M Y')}}</small>
	                </p>
	              </li>
	              <!-- Menu Footer-->
	              <li class="user-footer">
	                <div class="pull-left">
	                  <a href="{{ route('account') }}" class="btn btn-default btn-flat">Perfil</a>
	                </div>
	                <div class="pull-right">
	                	{!! Form::open(['url' => 'logout', 'method' => 'POST', 'role' => 'form'])!!}
	                  		<button type="submit" class="btn btn-default btn-flat">Salir</button>
	                	{!! Form::close()!!}
	                </div>
	              </li>
	            </ul>
			</li>
		</ul>
	</div>
</nav>