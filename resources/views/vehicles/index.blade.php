@extends('layouts.app')

@section('content')

	<h3>Mis Vehículos</h3>

	<a href="{{ route('mis-vehiculos.create')}}" class="btn btn-success">Agregar Vehículo</a>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Chapa</th>
				<th>Marca</th>
				<th>Color</th>
				<th>Modelo</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			@foreach ($vehicles as $vehicle)
				<tr data-id="{{ $vehicle->id }}">
					<td>{{ $vehicle->plate }}</td>
					<td>{{ $vehicle->brand }}</td>
					<td>{{ $vehicle->color }}</td>
					<td>{{ $vehicle->model }}</td>
					<td>
						<a href="{{ route('mis-vehiculos.edit', $vehicle->id)}}">Editar</a>
						<a href="#" class="btn-remove">Eliminar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	{!! $vehicles->links() !!}

	{!! Form::open(['route' => ['mis-vehiculos.destroy',':ROW_ID'], 'method' => 'DELETE', 'id' => 'form-delete']) !!}
	{!! Form::close() !!}

@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('.btn-remove').click(function(e){
			e.preventDefault();
			var row = $(this).parents('tr');
			var id = row.data('id');
			swal({
			  title: "Estas seguro de eliminar el vehículo?",
			  text: "Con esta acción los datos no podrán ser recuperados!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Eliminar!",
			  cancelButtonText: "Cancelar!"
			}).then(function(isConfirm){
				console.log(isConfirm);
			  if (isConfirm) {
				var form = $('#form-delete');
				var url = form.attr('action').replace(':ROW_ID',id);
				var data = form.serialize();
				var type = "";
				var title = "";
			  	$.post(url,data, function(result){
					if(result.success){
						row.fadeOut();
						type = "success";
						title = "Operación realizada!";
					}else{
						type = "error";
						title =  "No se pudo realizar la operación";
					}
					swal({   title: title,   text: result.message,   type: type,   confirmButtonText: "Aceptar" });
				}).fail(function (){
					swal('No se pudo realizar la petición.');
				});
			  } else {
			    swal("Cancelado", "El vehículo no ha sido eliminado", "error");
			  }
			});
		});
	});
</script>
@endsection