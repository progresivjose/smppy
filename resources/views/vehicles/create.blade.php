@extends('layouts.app')

@section('content')

	<h3>Agregar Vehiculo</h3>

	{!! Form::open(['route' => 'mis-vehiculos.store', 'method' => 'POST', 'role' => 'form']) !!}
		<div class="form-group">
			{!! Form::text('plate', '', ['class' => 'form-control', 'placeholder' => 'Chapa', 'required' => true] )!!}

			@if ($errors->has('plate'))
	        <span class="help-block">
	            <strong>{{ $errors->first('plate') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			{!! Form::text('brand', '', ['class' => 'form-control', 'placeholder' => 'Marca', 'required' => true] )!!}
			@if ($errors->has('brand'))
	        <span class="help-block">
	            <strong>{{ $errors->first('brand') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			{!! Form::text('color', '', ['class' => 'form-control', 'placeholder' => 'Color', 'required' => true] )!!}
			@if ($errors->has('color'))
	        <span class="help-block">
	            <strong>{{ $errors->first('color') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			{!! Form::text('model', '', ['class' => 'form-control', 'placeholder' => 'Modelo', 'required' => true] )!!}
			@if ($errors->has('model'))
	        <span class="help-block">
	            <strong>{{ $errors->first('model') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			{!! Form::text('chassis', '', ['class' => 'form-control', 'placeholder' => 'Chasis'] )!!}
			@if ($errors->has('chassis'))
	        <span class="help-block">
	            <strong>{{ $errors->first('chassis') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			{!! Form::text('document', '', ['class' => 'form-control', 'placeholder' => 'Cédula Verde', 'required' => true] )!!}
			@if ($errors->has('document'))
	        <span class="help-block">
	            <strong>{{ $errors->first('document') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			{!! Form::select('status', [1 => 'Activo', 2=> 'Inactivo'], 1, ['class' => 'form-control']) !!}
			@if ($errors->has('status'))
	        <span class="help-block">
	            <strong>{{ $errors->first('status') }}</strong>
	        </span>
	    	@endif
		</div>

		<div class="form-group">
			{!! Form::submit('Guardar', ['class' => 'btn btn-success'] )!!}
			<a href="{{ route('multas.index')}}" class="btn btn-danger pull-right">Cancelar</a>
		</div>

	{!! Form::close() !!}

@endsection