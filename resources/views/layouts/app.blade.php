<!DOCTYPE html>
<html lang="es">
  <head>
       <meta charset="utf-8">
       <title>SMPPy - Sistema Movil de Parking Paraguay</title>
       <link href="css/style.css" media="screen" rel="stylesheet">
       <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <meta name="viewport" content="width=device-width, initial-scale=1">
       <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
       <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
       <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.10.1/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
       <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
       <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
       <script src="{{ asset('js/Geolocalizacion.js') }}" type="text/javascript"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.10.1/sweetalert2.min.js" type="text/javascript"></script>

  </head>
  <body class="body">


  <div class="container">
    @include('partials.menu')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          @include('flash::message')

          @yield('content')

        </div>
    </div>
  </div>

    <footer>
      <a href="http://www.rodisenhos.com.py">Elaborado por RO-Diseños</a> - Raúl Obregón y Diego Martinez</a>
    </footer>

    @yield('scripts')

  </body>
</html>
