@extends('layouts.app')

@section('content')

	<h3>Zonas</h3>

	<a href="{{ route('zonas.create') }}" class="btn btn-success">Agregar Zona</a>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Calle 1</th>
				<th>Calle 2</th>
				<th>Calle 3</th>
				<th>Latitud</th>
				<th>Longitud</th>
				<th>Tipo</th>
				<th>Tarifa</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			@foreach ($zones as $zone)
				<tr data-id="{{ $zone->id }}">
					<td>{{ $zone->name }}</td>
					<td>{{ $zone->street1 }}</td>
					<td>{{ $zone->street2 }}</td>
					<td>{{ $zone->street3 }}</td>
					<td>{{ $zone->lat }}</td>
					<td>{{ $zone->lon }}</td>
					<td>{{ $zone->type }}</td>
					<td>{{ $zone->amount }}</td>
					<td>
						<a href="{{ route('zonas.edit', $zone->id)}}">Editar</a>
						<a href="#" class="btn-remove">Eliminar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	{!! $zones->links() !!}

	{!! Form::open(['route' => ['zonas.destroy',':ROW_ID'], 'method' => 'DELETE', 'id' => 'form-delete']) !!}
	{!! Form::close() !!}
@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('.btn-remove').click(function(e){
			e.preventDefault();
			var row = $(this).parents('tr');
			var id = row.data('id');
			swal({
			  title: "Estas seguro de eliminar la zona?",
			  text: "Con esta acción los datos no podrán ser recuperados!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Eliminar!",
			  cancelButtonText: "Cancelar!"
			}).then(function(isConfirm){
				console.log(isConfirm);
			  if (isConfirm) {
				var form = $('#form-delete');
				var url = form.attr('action').replace(':ROW_ID',id);
				var data = form.serialize();
				var type = "";
				var title = "";
			  	$.post(url,data, function(result){
					if(result.success){
						row.fadeOut();
						type = "success";
						title = "Operación realizada!";
					}else{
						type = "error";
						title =  "No se pudo realizar la operación";
					}
					swal({   title: title,   text: result.message,   type: type,   confirmButtonText: "Aceptar" });
				}).fail(function (){
					swal('No se pudo realizar la petición.');
				});
			  } else {
			    swal("Cancelado", "La zona no ha sido eliminada", "error");
			  }
			});
		});
	});
</script>
@endsection