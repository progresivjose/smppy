@extends('layouts.app')

@section('content')

<h3>Editar Zona</h3>

{!! Form::open(['route' => ['zonas.update', $zone->id], 'method' => 'PUT', 'role' => 'form'])!!}

	<div class="form-group">
		{!! Form::text('name', $zone->name, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required' => true]) !!}

		@if ($errors->has('name'))
	        <span class="help-block">
	            <strong>{{ $errors->first('name') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::text('street1', $zone->street1, ['class' => 'form-control', 'placeholder' => 'Calle 1', 'required' => true]) !!}

		@if ($errors->has('street1'))
	        <span class="help-block">
	            <strong>{{ $errors->first('street1') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::text('street2', $zone->street2, ['class' => 'form-control', 'placeholder' => 'Calle 2', 'required' => true]) !!}

		@if ($errors->has('street2'))
	        <span class="help-block">
	            <strong>{{ $errors->first('street2') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::text('street3', $zone->street3, ['class' => 'form-control', 'placeholder' => 'Calle 3', 'required' => true]) !!}

		@if ($errors->has('street3'))
	        <span class="help-block">
	            <strong>{{ $errors->first('street3') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::text('lat', $zone->lat, ['class' => 'form-control', 'placeholder' => 'Latitud']) !!}

		@if ($errors->has('lat'))
	        <span class="help-block">
	            <strong>{{ $errors->first('lat') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::text('lon', $zone->lon, ['class' => 'form-control', 'placeholder' => 'Longitud']) !!}

		@if ($errors->has('lon'))
	        <span class="help-block">
	            <strong>{{ $errors->first('lon') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::select('type', ['1' => 'Automovilista', '2' => 'Cuida Coches', '3' => 'PMT'],$zone->type, ['class' => 'form-control', 'placeholder' => '-- Elegir Tipo --', 'required' => true]) !!}

		@if ($errors->has('type'))
	        <span class="help-block">
	            <strong>{{ $errors->first('type') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::number('amount', $zone->amount, ['class' => 'form-control']) !!}

		@if ($errors->has('type'))
	        <span class="help-block">
	            <strong>{{ $errors->first('type') }}</strong>
	        </span>
	    @endif
	</div>

	<div class="form-group">
		{!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
		<a href="{{ route('zonas.index')}}" class="btn btn-danger pull-right">Cancelar</a>
	</div>

{!! Form::close() !!}

@endsection