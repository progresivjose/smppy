function getLocation() {
    console.log(navigator.geolocation);
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    }
    else {
        alert("La geolocalización no es soportada por este navegador.");
    }
}

function showPosition(position) {
    var gpslatitud = position.coords.latitude;
    var gpslongitud = position.coords.longitude;
    var contentla = document.getElementById("latitud");
    var contentlo = document.getElementById("longitud");

    contentla.value = gpslatitud;
    contentlo.value = gpslongitud;
}

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            window.alert("Se denego la geolocalización.")
            break;
        case error.POSITION_UNAVAILABLE:
            window.alert("La información de geolocalización no esta habilitada.")
            break;
        case error.TIMEOUT:
            window.alert("Se acabo el tiempo de espera para la geolocalización.")
            break;
        case error.UNKNOWN_ERROR:
            window.alert("Ocurrio un error no esperado.")
            break;
    }
}