$(document).ready(function(){
    $('#registerform').validate(
    {
     rules: {
       Nombre: {
         minlength: 2,
         required: true
       },
       CI: {
         minlength: 2,
         required: true
       },
       Razon: {
         minlength: 2,
         required: true
       },
       Ruc: {
         minlength: 2,
         required: true
       },
       Telefono: {
         minlength: 2,
         required: true
       },
       Direccion: {
         minlength: 2,
         required: true
       },
       Password: {
         minlength: 2,
         required: true
       },
       Email: {
         required: true,
         email: true
       },
       Apellido: {
         minlength: 2,
         required: true
       }
     },
     highlight: function(element) {
       $(element).text('Hay un error').closest('.control-group').removeClass('success').addClass('error');
     },
     success: function(element) {
       element
       .addClass('valid')
       .closest('.control-group').removeClass('error').addClass('success');
     }
    });
   }); // end document.ready