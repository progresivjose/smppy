<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => ['bloqued']], function(){
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/home', 'HomeController@index');
	Route::get('/perfil', 'UserController@account')->name('account');
	Route::put('/perfil', 'UserController@updateAccount')->name('update_account');
	Route::get('/comprar-minutos', 'TicketController@time')->name('time');
	Route::post('/comprar-minutos', 'TicketController@buyTime')->name('buy_time');
	Route::get('/cargar-billetera', 'TicketController@showWallet')->name('wallet');
	Route::post('/cargar-billetera', 'TicketController@buyWallet')->name('buy_wallet');
	Route::get('/comprar-ticket', 'TicketController@tickets')->name('tickets');
	Route::post('/comprar-ticket', 'TicketController@buyTicket')->name('buy_tickets');
	Route::resource('/mis-vehiculos', 'VehicleController');


	Route::group(['middleware' => ['role:admin,pmt'] ], function(){
		Route::resource('multas', 'PenaltyController');
	});

	Route::group(['middleware' => 'role:admin'], function(){
		Route::resource('zonas', 'ZoneController');
		Route::resource('usuarios', 'UserController');
		Route::resource('planes', 'PlanController');
		Route::resource('metodos', 'PaymentMethodController');
	});
});
